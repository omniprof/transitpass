package com.cejv416.transitpass;

import com.cejv416.transitpass.passes.BusPass;
import java.math.BigDecimal;

/**
 * The Transit Pass Exercise
 *
 * @author Ken Fogel
 * @version 0.1
 */
public class TransitPass {

    /**
     * Controller for the application
     */
    public void perform() {
        BusPass bp = new BusPass();
        bp.addMoney(new BigDecimal("25.0"));
        bp.determineBonus();
        bp.doReport("Buss Pass");
        bp.addRides(51);
        bp.determineBonus();
        bp.doReport("Bus Pass");
    }

    /**
     * Where it all begins
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new TransitPass().perform();
    }

}
