package com.cejv416.transitpass.data;

import java.math.BigDecimal;

/**
 * Data bean for the Transit Pass exercise
 *
 * @author Ken Fogel
 * @version 0.1
 */
public class TransitBean extends Object {

    private int numberOfRidesRemaining;

    private int numberOfRidesTakenThisMonth;
    private int numberOfRidesAddedThisMonth;
    private int numberOfBonusRidesAwardedThisMonth;

    private int numberOfRidesSinceActivation;
    private int numberOfBonusRidesSinceActivation;

    private BigDecimal moneyAddedThisMonth;
    private BigDecimal moneyAddedSinceActivation;

    public TransitBean() {
        moneyAddedThisMonth = BigDecimal.ZERO;
        moneyAddedSinceActivation = BigDecimal.ZERO;
    }

    public int getNumberOfRidesRemaining() {
        return numberOfRidesRemaining;
    }

    public void setNumberOfRidesRemaining(int numberOfRidesRemaining) {
        this.numberOfRidesRemaining = numberOfRidesRemaining;
    }

    public int getNumberOfRidesTakenThisMonth() {
        return numberOfRidesTakenThisMonth;
    }

    public void setNumberOfRidesTakenThisMonth(int numberOfRidesTakenThisMonth) {
        this.numberOfRidesTakenThisMonth = numberOfRidesTakenThisMonth;
    }

    public int getNumberOfRidesAddedThisMonth() {
        return numberOfRidesAddedThisMonth;
    }

    public void setNumberOfRidesAddedThisMonth(int numberOfRidesAddedThisMonth) {
        this.numberOfRidesAddedThisMonth = numberOfRidesAddedThisMonth;
    }

    public int getNumberOfBonusRidesAwardedThisMonth() {
        return numberOfBonusRidesAwardedThisMonth;
    }

    public void setNumberOfBonusRidesAwardedThisMonth(int numberOfBonusRidesAwardedThisMonth) {
        this.numberOfBonusRidesAwardedThisMonth = numberOfBonusRidesAwardedThisMonth;
    }

    public int getNumberOfRidesSinceActivation() {
        return numberOfRidesSinceActivation;
    }

    public void setNumberOfRidesSinceActivation(int numberOfRidesSinceActivation) {
        this.numberOfRidesSinceActivation = numberOfRidesSinceActivation;
    }

    public int getNumberOfBonusRidesSinceActivation() {
        return numberOfBonusRidesSinceActivation;
    }

    public void setNumberOfBonusRidesSinceActivation(int numberOfBonusRidesSinceActivation) {
        this.numberOfBonusRidesSinceActivation = numberOfBonusRidesSinceActivation;
    }

    public BigDecimal getMoneyAddedThisMonth() {
        return moneyAddedThisMonth;
    }

    public void setMoneyAddedThisMonth(BigDecimal moneyAddedThisMonth) {
        this.moneyAddedThisMonth = moneyAddedThisMonth;
    }

    public BigDecimal getMoneyAddedSinceActivation() {
        return moneyAddedSinceActivation;
    }

    public void setMoneyAddedSinceActivation(BigDecimal moneyAddedSinceActivation) {
        this.moneyAddedSinceActivation = moneyAddedSinceActivation;
    }

    // Object 
    @Override
    public String toString() {
        return "TransitBean{" + "numberOfRidesRemaining=" + numberOfRidesRemaining
                + "\nnumberOfRidesTakenThisMonth=" + numberOfRidesTakenThisMonth
                + "\nnumberOfRidesAddedThisMonth=" + numberOfRidesAddedThisMonth
                + "\nnumberOfBonusRidesThisMonth=" + numberOfBonusRidesAwardedThisMonth
                + "\nnumberOfRidesSinceActivation=" + numberOfRidesSinceActivation
                + "\nnumberOfBonusRidesSinceActivation=" + numberOfBonusRidesSinceActivation
                + "\nmoneyAddedThisMonth=" + moneyAddedThisMonth.toString()
                + "\nmoneyAddedSinceActivation=" + moneyAddedSinceActivation.toString() + "}\n";
    }
    
}
