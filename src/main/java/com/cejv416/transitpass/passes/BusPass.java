package com.cejv416.transitpass.passes;

/**
 * BusPass that specializes the TransitPass
 *
 * @author Ken Fogel
 * @version 0.1
 */
public class BusPass extends TransitPass {

    @Override
    public void determineBonus() {
        if (transitBean.getNumberOfRidesAddedThisMonth() > 50) {
            addBonusRides(3);
        }

    }

}
