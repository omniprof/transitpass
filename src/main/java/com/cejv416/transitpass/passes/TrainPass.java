package com.cejv416.transitpass.passes;

/**
 * TrainPass that specializes the TransitPass
 *
 * @author Ken Fogel
 * @version 0.1
 */
public class TrainPass extends TransitPass {

    @Override
    public void determineBonus() {
        if (transitBean.getNumberOfRidesAddedThisMonth() > 20) {
            addBonusRides(1);
        }

    }

}
