package com.cejv416.transitpass.passes;

import com.cejv416.transitpass.data.TransitBean;
import java.math.BigDecimal;

/**
 * Superclass for all Transit Passes
 *
 * @author Ken Fogel
 * @version 0.1
 */
public abstract class TransitPass {

    // protected so that the subclasses can access it
    protected TransitBean transitBean;

    /**
     * Constructor to instantiate a pass
     */
    public TransitPass() {
        transitBean = new TransitBean();
    }

    /**
     * Add money to what is already in the bean
     *
     * @param money
     */
    public void addMoney(BigDecimal money) {
        BigDecimal temp1 = transitBean.getMoneyAddedSinceActivation();
        BigDecimal temp2 = transitBean.getMoneyAddedThisMonth();

        transitBean.setMoneyAddedSinceActivation(temp1.add(money));
        transitBean.setMoneyAddedThisMonth(temp2.add(money));
    }

    /**
     * Add rides to what is already in the bean
     *
     * @param rides
     */
    public void addRides(int rides) {
        int temp1 = transitBean.getNumberOfRidesAddedThisMonth();
        int temp2 = transitBean.getNumberOfRidesSinceActivation();
        int temp3 = transitBean.getNumberOfRidesRemaining();

        transitBean.setNumberOfRidesAddedThisMonth(temp1 + rides);
        transitBean.setNumberOfRidesSinceActivation(temp2 + rides);
        transitBean.setNumberOfRidesRemaining(temp3 + rides);
    }

    /**
     * Add bonus rides to what is all ready in the bean
     *
     * @param rides
     */
    public void addBonusRides(int rides) {
        int temp1 = transitBean.getNumberOfBonusRidesAwardedThisMonth();
        int temp2 = transitBean.getNumberOfBonusRidesSinceActivation();

        transitBean.setNumberOfBonusRidesAwardedThisMonth(temp1 + rides);
        transitBean.setNumberOfBonusRidesSinceActivation(temp2 + rides);

        addRides(rides);
    }

    /**
     * Subtract a ride
     */
    public void subtractOneRide() {
        int temp1 = transitBean.getNumberOfRidesRemaining();

        transitBean.setNumberOfRidesRemaining(temp1 - 1);
    }

    /**
     * Display the monthly report
     */
    public void doReport(String passType) {
        System.out.println(passType + " Report\n" + transitBean.toString());
    }

    /**
     * Reset the monthly values in the TransitBean
     */
    public void doEndOfMonthReset() {
        transitBean.setMoneyAddedThisMonth(BigDecimal.ZERO);
        transitBean.setNumberOfBonusRidesAwardedThisMonth(0);
        transitBean.setNumberOfRidesTakenThisMonth(0);
        transitBean.setNumberOfRidesAddedThisMonth(0);
    }
    
    /**
     * Abstract method now requires every sub class to implement this method.
     */
    public abstract void determineBonus();
}
